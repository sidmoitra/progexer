# Programming Exercise 1 - HW/SW Code Design
## University of Paderborn
### Prof. Christian Plessl

The programming exercise can be found [here](http://homepages.uni-paderborn.de/plessl/teaching/2016-Codesign/exercises/ProgrammingExercise1.pdf)

Requirements to run this code:

  - LP Solver for solving Integer Linear Program (ILP)
  - Java runtime JRE1.8 or newer (This won't run on JRE7 because the LPSolver wrapper uses JDK8 for compilation)
  - JNI library to execute LP Solver from Java program (LP Solver is a C-based library and is not Object-Oriented. So I use this wrapper)

### Where to get LP Solver:

You need LP Solver installed on your system.

- For Linux (Ubuntu) :
```sh
$ sudo apt install lp-solve
```
- For MacOS :  
```sh
$ brew tap homebrew/science
$ brew install lp_solve
```
- For Windows : See this [link](http://www.lfd.uci.edu/~gohlke/pythonlibs/#lp_solve)

### Where to get JNI wrapper for lp_solve

This is only needed for executing lp_solver from Java programs. See link for details - http://lpsolve.sourceforge.net/5.5/Java/README.html

The wrappers can be downloaded from jni/ folder of this project. Just choose the option for your machine. These wrappers are machine specific. You must also place these wrappers to specific folders only.

- On Windows, copy the wrapper stub library lpsolve55j.dll to the directory that already contains lpsolve55.dll.

- On Linux, copy the wrapper stub library liblpsolve55j.so to the directory that already contains liblpsolve55.so. Run ldconfig to include the library in the shared libray cache. (On my machine this is: /usr/lib/)

### How to run the JAR
Once done, you can run the executable jar. the latest build is present in dist/ folder of this project:
```sh
$ java -jar progexer-0.1.jar {0} {1}
```

The jar expects 2 parameters:

- {0} (Required): The .txt file complete path specifying the problem description
- {1} (Optional): The mode in which you wish to see the results. Valid values - UI or CONSOLE. If you don't specify this, the results will be shown on Console by default.
- 
Example commands:
```sh
$ java -jar progexer-0.1.jar /home/sid/prblm_desc.txt UI
$ java -jar progexer-0.1.jar /home/sid/prblm_desc.txt CONSOLE
$ java -jar progexer-0.1.jar /home/sid/prblm_desc.txt
```

### LP Solver file generation
The code also generates LP Solver file (in .lp format). It is always generated in the temp/ folder of your machine. While executing the jar, you will note the location on the console (if execution ends without exception).

### Screenshots of UI Results

- ASAP/ALAP Scheduling without resource constraints
![Scheduling_ASAP.png](https://bitbucket.org/repo/qBxLzR/images/1856949539-Scheduling_ASAP.png)

- List Scheduling with resource constraints
![Scheduling_List.png](https://bitbucket.org/repo/qBxLzR/images/924759033-Scheduling_List.png)

- Optimal Scheduling using ILP with resource constraints
![Scheduling_ILP.png](https://bitbucket.org/repo/qBxLzR/images/514349355-Scheduling_ILP.png)

- Optimal Scheduling Design Space Exploration using Pareto points
![Scheduling_Optimal.png](https://bitbucket.org/repo/qBxLzR/images/2652742908-Scheduling_Optimal.png)