/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer;

import java.io.File;

import com.sid.codedesign.progexer.core.parse.DefaultParser;
import com.sid.codedesign.progexer.core.parse.ProblemDescription;
import com.sid.codedesign.progexer.core.schedule.ALAPScheduler;
import com.sid.codedesign.progexer.core.schedule.ASAPScheduler;
import com.sid.codedesign.progexer.core.schedule.DesignSpaceExploration;
import com.sid.codedesign.progexer.core.schedule.ILPScheduler;
import com.sid.codedesign.progexer.core.schedule.ListScheduler;
import com.sid.codedesign.progexer.core.schedule.SchedulerUtils;
import com.sid.codedesign.progexer.core.schedule.model.OperationNode;
import com.sid.codedesign.progexer.core.schedule.model.ScheduleResult;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Siddhartha Moitra
 */
public class Main {

    public static void main(String[] args) throws CloneNotSupportedException, IOException {
        String fileLocation = args[0];
        String mode = null;
        try {
            mode = args[1];
        } catch (ArrayIndexOutOfBoundsException e) {
            mode = null;
        }

        if (mode != null && mode.equalsIgnoreCase("ui")) {
            mode = "GUI";
        } else {
            mode = "CONSOLE";
        }

        final DefaultParser fileParser = new DefaultParser();
        ProblemDescription problemDesc = null;
        final ScheduleResult scheduleResult = new ScheduleResult();
        
        File file = null;
        if (fileLocation == null || fileLocation.isEmpty()) {
            System.err.println("No problem description file defined.");
            System.err.println("Will exit now...");
            System.exit(1);
        } else if (fileLocation.startsWith("sample_test")) {
            List<String> lines = load_test_files(fileLocation);
            System.out.println("Input File Location: " + fileLocation);
            System.out.println("Output Mode: " + mode);
            System.out.println("ILP Solver: LP_SOLVER");
            System.out.println("\n");
            
            problemDesc = fileParser.parseProblemDescription(lines.toArray(new String[0]));
            scheduleResult.setProblemDescriptionFile(fileLocation);
        } else {
            file = new File(fileLocation);
            if (!file.exists() || !file.isFile() || !file.canRead()) {
                System.err.println("Either file does not exist or cannot be read. Please check permissions.");
                System.err.println("Will exit now...");
                System.exit(1);
            }

            System.out.println("Input File Location: " + file.getAbsolutePath());
            System.out.println("Output Mode: " + mode);
            System.out.println("ILP Solver: LP_SOLVER");
            System.out.println("\n");
            
            problemDesc = fileParser.parseProblemDescription(file);
            scheduleResult.setProblemDescriptionFile(file.getAbsolutePath());
        }

        // ASAP Scheduling
        final OperationNode asapRoot = SchedulerUtils.createTree(problemDesc.getDependencies());
        final ASAPScheduler asapScheduler = new ASAPScheduler(problemDesc, asapRoot);
        asapScheduler.executeSchedule(false);
        // ALAP Scheduling
        final OperationNode alapRoot = SchedulerUtils.cloneTree(asapScheduler.getRoot());
        final ALAPScheduler alapScheduler = new ALAPScheduler(problemDesc, alapRoot);
        alapScheduler.executeSchedule(false);
        // List Scheduling
        final OperationNode listScheduleRoot = SchedulerUtils.cloneTree(alapScheduler.getRoot());
        final ListScheduler listScheduler = new ListScheduler(problemDesc, listScheduleRoot);
        listScheduler.executeSchedule(true);
        // ILP Scheduling
        final OperationNode ilpScheduleRoot = SchedulerUtils.cloneTree(listScheduler.getRoot());
        final ILPScheduler ilpScheduler = new ILPScheduler(problemDesc, ilpScheduleRoot);
        ilpScheduler.executeSchedule(true);
        // Design Exploration
        final OperationNode designSpaceExplorationRoot = SchedulerUtils.cloneTree(alapScheduler.getRoot());
        final DesignSpaceExploration designSpaceExploration = new DesignSpaceExploration(problemDesc, designSpaceExplorationRoot);
        designSpaceExploration.executeSchedule();
        System.out.println("Start: Calculating Pareto Optimal points...");
        SchedulerUtils.calculateParetoOptimal(designSpaceExploration.getExplorationResults());

        System.out.println("Processing complete. Will now show results...");

        scheduleResult.setProblemDescription(problemDesc);
        scheduleResult.setAsapSchedule(SchedulerUtils.getNodesBFS(asapScheduler.getRoot()));
        scheduleResult.setAlapSchedule(SchedulerUtils.getNodesBFS(alapScheduler.getRoot()));
        scheduleResult.setListSchedule(SchedulerUtils.getNodesBFS(listScheduler.getRoot()));
        scheduleResult.setIlpSchedule(SchedulerUtils.getNodesBFS(ilpScheduler.getRoot()));
        scheduleResult.setExplorationResults(designSpaceExploration.getExplorationResults());

        if (mode.equals("GUI")) {
            ResultUI.showResults(scheduleResult);
        } else {
            ResultConsole.showResults(scheduleResult);
        }
    }

    private static List<String> load_test_files(String testFile) throws IOException {
        List<String> lines = new ArrayList<>();
        InputStream is = null;
        if (testFile.equalsIgnoreCase("sample_test_1")) {
            is = Main.class.getResourceAsStream(File.separator + "prblm_desc_2.txt");
        } else if (testFile.equalsIgnoreCase("sample_test_2")) {
            is = Main.class.getResourceAsStream(File.separator + "prblm_desc_2.txt");
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;

        while ((line = br.readLine()) != null) {
            lines.add(line);
        }
        return lines;
    }
}
