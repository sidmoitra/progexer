/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer;

import com.sid.codedesign.progexer.core.parse.ProblemDescription;
import com.sid.codedesign.progexer.core.parse.Resource;
import com.sid.codedesign.progexer.core.parse.ResourceType;
import com.sid.codedesign.progexer.core.schedule.model.DesignExplorationResult;
import com.sid.codedesign.progexer.core.schedule.model.OperationNode;
import java.util.List;

import com.sid.codedesign.progexer.core.schedule.model.ScheduleResult;

import dnl.utils.text.table.TextTable;

/**
 *
 * @author Siddhartha Moitra
 */
public class ResultConsole {

    public static void showResults(ScheduleResult scheduleResult) {
        // Show in console
        showInputAnalysis(scheduleResult.getProblemDescription());
        System.out.println("\n");
        System.out.println("Results:");
        System.out.println("\n");
        showResultsASAP(scheduleResult);
        System.out.println("\n");
        showResultsALAP(scheduleResult);
        System.out.println("\n");
        showResultsList(scheduleResult);
        System.out.println("\n");
        showResultsILP(scheduleResult);
        System.out.println("\n");
        showResultsDesignSpace(scheduleResult);
        System.out.println("\n");
    }

    private static void showInputAnalysis(ProblemDescription problemDesc) {
        System.out.println("Input and Processing Analysis");
        System.out.println("-----------------------------");
        System.out.println("Problem Description File:");
        System.out.println("Number of resources: " + problemDesc.getResources().size());
        System.out.println("Number of operations: " + problemDesc.getOperations().size());
        System.out.println("Number of connections: " + problemDesc.getDependencies().size());

    }
    
    private static void showCommonResults(List<OperationNode> nodes) {
        System.out.println("Latency: " + (nodes.get(nodes.size() - 1).getOperationStartTime() - nodes.get(0).getOperationStartTime()));
    }

    private static void showResultsASAP(ScheduleResult scheduleResult) {
    	List<OperationNode> nodes = scheduleResult.getAsapSchedule();
    	
        System.out.println("ASAP Scheduling (without resource constraints)");
        System.out.println("----------------------------------------------");
        showCommonResults(nodes);
        String[] columnNames = {"Operation", "Operation Type", "Start Time", "End Time"};
        Object[][] data = new Object[nodes.size()][4];
        for (int i = 0; i < nodes.size(); i++) {
            OperationNode node = nodes.get(i);
            data[i][0] = node.getOperationName();
            data[i][1] = node.getOperationType();
            data[i][2] = new Integer(node.getOperationStartTime()).toString();
            data[i][3] = new Integer(node.getOperationEndTime()).toString();
        }
        TextTable tt = new TextTable(columnNames, data);
        // this adds the numbering on the left 
        tt.setAddRowNumbering(true);
        tt.printTable();
    }

    private static void showResultsALAP(ScheduleResult scheduleResult) {
    	List<OperationNode> nodes = scheduleResult.getAlapSchedule();
        
        System.out.println("ALAP Scheduling (without resource constraints)");
        System.out.println("----------------------------------------------");
        showCommonResults(nodes);
        String[] columnNames = {"Operation", "Operation Type", "Start Time", "End Time", "Slack"};
        Object[][] data = new Object[nodes.size()][5];
        for (int i = 0; i < nodes.size(); i++) {
            OperationNode node = nodes.get(i);
            data[i][0] = node.getOperationName();
            data[i][1] = node.getOperationType();
            data[i][2] = new Integer(node.getOperationStartTime()).toString();
            data[i][3] = new Integer(node.getOperationEndTime()).toString();
            data[i][4] = new Integer(node.getSlack()).toString();
        }
        TextTable tt = new TextTable(columnNames, data);
        // this adds the numbering on the left 
        tt.setAddRowNumbering(true);
        tt.printTable();
    }
    
    private static void showResultsList(ScheduleResult scheduleResult) {
    	List<OperationNode> nodes = scheduleResult.getListSchedule();
        
        System.out.println("List Scheduling (with resource constraints)");
        System.out.println("-------------------------------------------");
        showCommonResults(nodes);
        String[] columnNames = {"Operation", "Operation Type", "Start Time", "End Time"};
        Object[][] data = new Object[nodes.size()][4];
        for (int i = 0; i < nodes.size(); i++) {
            OperationNode node = nodes.get(i);
            data[i][0] = node.getOperationName();
            data[i][1] = node.getOperationType();
            data[i][2] = new Integer(node.getOperationStartTime()).toString();
            data[i][3] = new Integer(node.getOperationEndTime()).toString();
        }
        TextTable tt = new TextTable(columnNames, data);
        // this adds the numbering on the left 
        tt.setAddRowNumbering(true);
        tt.printTable();
    }
    
    private static void showResultsILP(ScheduleResult scheduleResult) {
    	List<OperationNode> nodes = scheduleResult.getIlpSchedule();
    	
        System.out.println("Resource Constraint Scheduling (using ILP)");
        System.out.println("------------------------------------------");
        showCommonResults(nodes);
        String[] columnNames = {"Operation", "Operation Type", "Start Time", "End Time"};
        Object[][] data = new Object[nodes.size()][4];
        for (int i = 0; i < nodes.size(); i++) {
            OperationNode node = nodes.get(i);
            data[i][0] = node.getOperationName();
            data[i][1] = node.getOperationType();
            data[i][2] = new Integer(node.getOperationStartTime()).toString();
            data[i][3] = new Integer(node.getOperationEndTime()).toString();
        }
        TextTable tt = new TextTable(columnNames, data);
        // this adds the numbering on the left 
        tt.setAddRowNumbering(true);
        tt.printTable();
    }
    
    private static void showResultsDesignSpace(ScheduleResult scheduleResult) {
    	List<DesignExplorationResult> results = scheduleResult.getExplorationResults();
    	
    	System.out.println("Design Space Exploration");
        System.out.println("------------------------");
        
        String[] columnNames = {"Resource Allocation", "Total Cost", "Total Execution Time", "Is-Pareto Optimal"};
        Object[][] data = new Object[results.size()][4];
        for (int i = 0; i < results.size(); i++) {
        	DesignExplorationResult result = results.get(i);
        	StringBuilder sb = new StringBuilder();
        	for(Resource res: result.getResources()) {
        		if(res.getType() == ResourceType.NOP)
        			continue;
        		sb.append(res.getType()).append(":").append(res.getAllocation()).append("; ");
        	}
            data[i][0] = sb.toString();
            data[i][1] = new Integer(result.getCost()).toString();
            data[i][2] = new Integer(result.getLatency()).toString();
            data[i][3] = new Boolean(result.isIsParetoOptimal()).toString();
        }
        TextTable tt = new TextTable(columnNames, data);
        // this adds the numbering on the left 
        tt.setAddRowNumbering(true);
        tt.printTable();
    }
}
