/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer;

import com.sid.codedesign.progexer.core.parse.Resource;
import com.sid.codedesign.progexer.core.parse.ResourceType;
import com.sid.codedesign.progexer.core.schedule.model.DesignExplorationResult;
import com.sid.codedesign.progexer.core.schedule.model.OperationEdge;
import com.sid.codedesign.progexer.core.schedule.model.OperationNode;
import com.sid.codedesign.progexer.core.schedule.model.ScheduleResult;
import java.awt.Color;
import java.awt.Shape;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.DateTickUnitType;

import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.time.Hour;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.util.ShapeUtilities;
import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.DataSizeAction;
import prefuse.action.layout.graph.NodeLinkTreeLayout;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.EdgeRenderer;
import prefuse.render.LabelRenderer;
import prefuse.util.ColorLib;
import prefuse.visual.VisualItem;

/**
 *
 * @author Siddhartha Moitra
 */
public class ResultUI extends javax.swing.JPanel {

    private final ScheduleResult scheduleResult;

    private class DFGDisplay extends Display {

        private static final String SIZE = "size";
        private static final String LABEL = "label";
        private static final int SIZE_VAL = 60;
        private static final String GRAPH = "graph";
        private static final String NODES = "graph.nodes";
        private static final String EDGES = "graph.edges";
        private Graph graph;

        private DFGDisplay() {
            super(new Visualization());
            initGraph();

            //  standard labelRenderer for the given label
            LabelRenderer nodeRenderer = new LabelRenderer(LABEL);
            EdgeRenderer edgeRenderer = new EdgeRenderer(Constants.EDGE_TYPE_LINE, Constants.EDGE_ARROW_FORWARD);
            edgeRenderer.setArrowType(Constants.EDGE_ARROW_FORWARD);
            edgeRenderer.setArrowHeadSize(10, 10);
            // rendererFactory for the visualization items 
            DefaultRendererFactory rendererFactory = new DefaultRendererFactory(nodeRenderer, edgeRenderer);
            // set the labelRenderer
            rendererFactory.setDefaultRenderer(nodeRenderer);
            m_vis.setRendererFactory(rendererFactory);

            // Color Actions
            ColorAction nodeText = new ColorAction(NODES, VisualItem.TEXTCOLOR);
            nodeText.setDefaultColor(ColorLib.gray(0));
            ColorAction nodeStroke = new ColorAction(NODES, VisualItem.STROKECOLOR);
            nodeStroke.setDefaultColor(ColorLib.gray(100));
            ColorAction nodeFill = new ColorAction(NODES, VisualItem.FILLCOLOR);
            nodeFill.setDefaultColor(ColorLib.gray(255));
            ColorAction edgeStrokes = new ColorAction(EDGES, VisualItem.STROKECOLOR);
            edgeStrokes.setDefaultColor(ColorLib.gray(100));

            // bundle the color actions
            ActionList draw = new ActionList();
            draw.add(nodeText);
            draw.add(nodeStroke);
            draw.add(nodeFill);
            draw.add(edgeStrokes);

            // DataSizeAction
            DataSizeAction nodeDataSizeAction = new DataSizeAction(NODES, SIZE);
            draw.add(nodeDataSizeAction);
            m_vis.putAction("draw", draw);

            // create the layout action for the graph
            NodeLinkTreeLayout treeLayout = new NodeLinkTreeLayout(GRAPH);
            treeLayout.setOrientation(Constants.ORIENT_TOP_BOTTOM);
            m_vis.putAction("treeLayout", treeLayout);

            // run actions
            m_vis.run("treeLayout");
            m_vis.run("draw");
        }

        private void recursivelyCreateGraph(Map<String, Node> addedNodeMap, OperationNode current) {
            Node currentGraphNode = addedNodeMap.get(current.getOperationName());
            // For every child do
            for (OperationEdge edge : current.getEdges()) {
                OperationNode child = edge.getSink();
                Node childGraphNode = null;
                if (!addedNodeMap.containsKey(child.getOperationName())) {
                    childGraphNode = addNode(child.getOperationName() + " (" + child.getOperationType() + ")");
                    addedNodeMap.put(child.getOperationName(), childGraphNode);
                } else {
                    childGraphNode = addedNodeMap.get(child.getOperationName());
                }
                graph.addEdge(currentGraphNode, childGraphNode);
                recursivelyCreateGraph(addedNodeMap, child);
            }

        }

        private void initGraph() {
            graph = new Graph(true);
            // first define a column
            graph.addColumn(LABEL, String.class);
            graph.addColumn(SIZE, int.class);
            // then add nodes
            Map<String, Node> addedNodeMap = new HashMap<>();
            List<OperationNode> nodeList = scheduleResult.getAsapSchedule();
            for (OperationNode node : nodeList) {
                Node graphNode = null;
                if (!addedNodeMap.containsKey(node.getOperationName())) {
                    graphNode = addNode(node.getOperationName() + " (" + node.getOperationType() + ")");
                    addedNodeMap.put(node.getOperationName(), graphNode);
                    recursivelyCreateGraph(addedNodeMap, node);
                }
            }
            // then add the edges

            m_vis.addGraph(GRAPH, graph);

        }

        private Node addNode(String label) {
            Node node = graph.addNode();
            node.setString(LABEL, label);
            node.setInt(SIZE, SIZE_VAL);
            return node;
        }
    }

    public static void showResults(final ScheduleResult scheduleResult) {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //Create and set up the window.
                JFrame frame = new JFrame("Scheduling Results");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                //Create and set up the content pane.
                ResultUI newContentPane = new ResultUI(scheduleResult);
                newContentPane.setOpaque(true); //content panes must be opaque
                JScrollPane scroll = new JScrollPane(newContentPane);
                frame.setContentPane(scroll);

                //Display the window.
                frame.pack();
                frame.setVisible(true);
            }
        });
    }

    /**
     * Creates new form ResultUI
     *
     * @param sc
     */
    public ResultUI(ScheduleResult sc) {
        this.scheduleResult = sc;

        initComponents();
        init_input_analytics_pane();

        panelDFG.add(new DFGDisplay());

        init_asap_alap_table();
        init_asap_alap_chart();

        init_list_table();
        init_list_chart();

        init_ilp_table();
        init_ilp_chart();

        init_design_space_exploration_table();
        init_design_space_exploration_chart();
    }
    
    private TaskSeries getASAPSeriesGantt() {
        TaskSeries ts = new TaskSeries("ASAP Schedule");
        for (OperationNode op : scheduleResult.getAsapSchedule()) {
            if (op.getOperationType() != ResourceType.NOP) {
                StringBuilder sb = new StringBuilder();
                sb.append(op.getOperationName()).append("(").append(op.getOperationType()).append(")");
                ts.add(new Task(sb.toString(), new SimpleTimePeriod(
                        new Hour(op.getOperationStartTime(), 1, 1, 2016).getFirstMillisecond(), 
                        new Hour(op.getOperationEndTime(), 1, 1, 2016).getFirstMillisecond())));
            }
        }
        return ts;
    }

    private TaskSeries getALAPSeriesGantt() {
        TaskSeries ts = new TaskSeries("ALAP Schedule");
        for (OperationNode op : scheduleResult.getAlapSchedule()) {
            if (op.getOperationType() != ResourceType.NOP) {
                StringBuilder sb = new StringBuilder();
                sb.append(op.getOperationName()).append("(").append(op.getOperationType()).append(")");
                ts.add(new Task(sb.toString(), new SimpleTimePeriod(
                        new Hour(op.getOperationStartTime(), 1, 1, 2016).getFirstMillisecond(), 
                        new Hour(op.getOperationEndTime(), 1, 1, 2016).getFirstMillisecond())));
            }
        }
        return ts;
    }
    
    private TaskSeries getListSeriesGantt() {
        TaskSeries ts = new TaskSeries("List Schedule");
        for (OperationNode op : scheduleResult.getListSchedule()) {
            if (op.getOperationType() != ResourceType.NOP) {
                StringBuilder sb = new StringBuilder();
                sb.append(op.getOperationName()).append("(").append(op.getOperationType()).append(")");
                ts.add(new Task(sb.toString(), new SimpleTimePeriod(
                        new Hour(op.getOperationStartTime(), 1, 1, 2016).getFirstMillisecond(), 
                        new Hour(op.getOperationEndTime(), 1, 1, 2016).getFirstMillisecond())));
            }
        }
        return ts;
    }

    private TaskSeries getIlpSeriesGantt() {
        TaskSeries ts = new TaskSeries("Resource-constraint Schedule (ILP)");
        for (OperationNode op : scheduleResult.getIlpSchedule()) {
            if (op.getOperationType() != ResourceType.NOP) {
                StringBuilder sb = new StringBuilder();
                sb.append(op.getOperationName()).append("(").append(op.getOperationType()).append(")");
                ts.add(new Task(sb.toString(), new SimpleTimePeriod(
                        new Hour(op.getOperationStartTime(), 1, 1, 2016).getFirstMillisecond(), 
                        new Hour(op.getOperationEndTime(), 1, 1, 2016).getFirstMillisecond())));
            }
        }
        return ts;
    }
    
    private void init_input_analytics_pane() {
        String[] columnNames = {"",
            "ASAP: Op_Start"};
        Object[][] data = {{"Problem Description File:", scheduleResult.getProblemDescriptionFile()},
        {"Number of Resources:", Integer.toString(scheduleResult.getProblemDescription().getResources().size())},
        {"Number of Operations:", Integer.toString(scheduleResult.getProblemDescription().getResources().size())},
        {"Number of Edges:", Integer.toString(scheduleResult.getProblemDescription().getDependencies().size())}};

        final JTable table = new JTable(data, columnNames);
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        table.setFillsViewportHeight(true);

        panelInputAnalytics.add(table);
    }

    private void init_asap_alap_table() {
        String[] columnNames = {"Operation",
            "ASAP: Op_Start",
            "ASAP: Op_End",
            "Slack",
            "ALAP: Op_Start",
            "ASAP: Op_End"};

        int numberOfOperations = scheduleResult.getProblemDescription().getOperations().size();
        Object[][] data = new Object[numberOfOperations][6];
        for (int i = 0; i < numberOfOperations; i++) {
            String asapOp = scheduleResult.getAsapSchedule().get(i).getOperationName();
            String alapOp = scheduleResult.getAlapSchedule().get(i).getOperationName();
            if (!asapOp.equals(alapOp)) {
                throw new RuntimeException("ASAP and ALAP Schedule must be sorted using same comparator.");
            }

            data[i][0] = asapOp + " (" + scheduleResult.getAsapSchedule().get(i).getOperationType() + ")";
            data[i][1] = Integer.toString(scheduleResult.getAsapSchedule().get(i).getOperationStartTime());
            data[i][2] = Integer.toString(scheduleResult.getAsapSchedule().get(i).getOperationEndTime());
            data[i][3] = Integer.toString(scheduleResult.getAlapSchedule().get(i).getSlack());
            data[i][4] = Integer.toString(scheduleResult.getAlapSchedule().get(i).getOperationStartTime());
            data[i][5] = Integer.toString(scheduleResult.getAlapSchedule().get(i).getOperationEndTime());
        }

        final JTable table = new JTable(data, columnNames);
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        table.setFillsViewportHeight(true);

        JScrollPane sp = new JScrollPane(table);
        panel_ASAP_ALAP.add(sp);
    }

    private void init_asap_alap_chart() {
        // Create dataset
        final TaskSeriesCollection collection = new TaskSeriesCollection();
        collection.add(getASAPSeriesGantt());
        collection.add(getALAPSeriesGantt());
        //collection.add(getListSeriesGantt());

        final JFreeChart chart = ChartFactory.createGanttChart(
                "ASAP/ALAP Scheduling", // chart title
                "Operation", // domain axis label
                "Memory Cycle", // range axis label
                collection, // data
                true, // include legend
                false, // tooltips
                false // urls
        );
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        DateAxis range = new DateAxis("Memory Cycle");
        range.setDateFormatOverride(new SimpleDateFormat("HH"));
        range.setTickUnit(new DateTickUnit(DateTickUnitType.HOUR, 1));
        plot.setRangeAxis(range);

        final ChartPanel chartPanel = new ChartPanel(chart);
        panel_ASAP_ALAP.add(chartPanel);
    }

    private void init_list_table() {
        String[] columnNames = {"Operation",
            "Op_Start",
            "Op_End"};

        int numberOfOperations = scheduleResult.getProblemDescription().getOperations().size();
        Object[][] data = new Object[numberOfOperations][3];
        for (int i = 0; i < numberOfOperations; i++) {
            data[i][0] = scheduleResult.getListSchedule().get(i).getOperationName() + " (" + scheduleResult.getListSchedule().get(i).getOperationType() + ")";;
            data[i][1] = Integer.toString(scheduleResult.getListSchedule().get(i).getOperationStartTime());
            data[i][2] = Integer.toString(scheduleResult.getListSchedule().get(i).getOperationEndTime());
        }

        final JTable table = new JTable(data, columnNames);
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        table.setFillsViewportHeight(true);

        JScrollPane sp = new JScrollPane(table);
        panelListSchedule.add(sp);
    }

    private void init_list_chart() {
        // Create dataset
        final TaskSeriesCollection collection = new TaskSeriesCollection();
        collection.add(getASAPSeriesGantt());
        collection.add(getALAPSeriesGantt());
        collection.add(getListSeriesGantt());

        final JFreeChart chart = ChartFactory.createGanttChart(
                "List Scheduling (Comparison)", // chart title
                "Operation", // domain axis label
                "Memory Cycle", // range axis label
                collection, // data
                true, // include legend
                false, // tooltips
                false // urls
        );
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        DateAxis range = new DateAxis("Memory Cycle");
        range.setDateFormatOverride(new SimpleDateFormat("HH"));
        range.setTickUnit(new DateTickUnit(DateTickUnitType.HOUR, 1));
        plot.setRangeAxis(range);

        final ChartPanel chartPanel = new ChartPanel(chart);
        panelListSchedule.add(chartPanel);
    }

    private void init_ilp_table() {
        String[] columnNames = {"Operation",
            "Op_Start",
            "Op_End"};

        int numberOfOperations = scheduleResult.getProblemDescription().getOperations().size();
        Object[][] data = new Object[numberOfOperations][3];
        for (int i = 0; i < numberOfOperations; i++) {
            data[i][0] = scheduleResult.getIlpSchedule().get(i).getOperationName() + " (" + scheduleResult.getIlpSchedule().get(i).getOperationType() + ")";;
            data[i][1] = Integer.toString(scheduleResult.getIlpSchedule().get(i).getOperationStartTime());
            data[i][2] = Integer.toString(scheduleResult.getIlpSchedule().get(i).getOperationEndTime());
        }

        final JTable table = new JTable(data, columnNames);
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        table.setFillsViewportHeight(true);

        JScrollPane sp = new JScrollPane(table);
        panelILPSchedule.add(sp);
    }

    private void init_ilp_chart() {
        // Create dataset
        final TaskSeriesCollection collection = new TaskSeriesCollection();
        collection.add(getListSeriesGantt());
        collection.add(getIlpSeriesGantt());

        final JFreeChart chart = ChartFactory.createGanttChart(
                "Resource-constraint Scheduling (Comparison: List and ILP)", // chart title
                "Operation", // domain axis label
                "Memory Cycle", // range axis label
                collection, // data
                true, // include legend
                false, // tooltips
                false // urls
        );
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        DateAxis range = new DateAxis("Memory Cycle");
        range.setDateFormatOverride(new SimpleDateFormat("HH"));
        range.setTickUnit(new DateTickUnit(DateTickUnitType.HOUR, 1));
        plot.setRangeAxis(range);

        final ChartPanel chartPanel = new ChartPanel(chart);
        panelILPSchedule.add(chartPanel);
    }

    private void init_design_space_exploration_table() {
        String[] columnNames = {"Plot Points(X,Y)",
            "MUL Allocation",
            "ALU Allocation"};

        int numberOfResults = scheduleResult.getExplorationResults().size();
        Object[][] data = new Object[numberOfResults][3];
        for (int i = 0; i < numberOfResults; i++) {
            DesignExplorationResult result = scheduleResult.getExplorationResults().get(i);
            data[i][0] = "Point( " + result.getCost() + " , " + result.getLatency() + " )";
            for (Resource resource : result.getResources()) {
                if (resource.getType() == ResourceType.MUL) {
                    data[i][1] = resource.getAllocation();
                }
            }
            for (Resource resource : result.getResources()) {
                if (resource.getType() == ResourceType.ALU) {
                    data[i][2] = resource.getAllocation();
                }
            }
        }

        final JTable table = new JTable(data, columnNames);
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        table.setFillsViewportHeight(true);

        JScrollPane sp = new JScrollPane(table);
        panelDesignSpaceExploration.add(sp);
    }

    private void init_design_space_exploration_chart() {
        final XYSeries paretoOptimal = new XYSeries("Pareto-Optimal");
        for (DesignExplorationResult result : scheduleResult.getExplorationResults()) {
            if (result.isIsParetoOptimal()) {
                paretoOptimal.add(result.getCost(), result.getLatency());
            }
        }

        final XYSeries paretoNonOptimal = new XYSeries("Pareto-Non Optimal");
        for (DesignExplorationResult result : scheduleResult.getExplorationResults()) {
            if (!result.isIsParetoOptimal()) {
                paretoNonOptimal.add(result.getCost(), result.getLatency());
            }
        }
        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(paretoOptimal);
        dataset.addSeries(paretoNonOptimal);

        JFreeChart xylineChart = ChartFactory.createScatterPlot(
                "Optimal Design Points",
                "Cost",
                "ExecutionTime",
                dataset,
                PlotOrientation.VERTICAL,
                true, // include legend
                true, // tooltips
                false // urls
        );
        // Shape of the points
        Shape cross = ShapeUtilities.createDiagonalCross(3, 1);
        XYPlot xyPlot = (XYPlot) xylineChart.getPlot();
        xyPlot.setDomainCrosshairVisible(true);
        xyPlot.setRangeCrosshairVisible(true);
        XYItemRenderer renderer = xyPlot.getRenderer();
        renderer.setSeriesShape(0, cross);
        renderer.setSeriesPaint(0, Color.red);

        ChartPanel chartPanel = new ChartPanel(xylineChart);
        panelDesignSpaceExploration.add(chartPanel);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel7 = new javax.swing.JLabel();
        javax.swing.JLabel jLabel1 = new javax.swing.JLabel();
        javax.swing.JLabel jLabel4 = new javax.swing.JLabel();
        panelInputAnalytics = new javax.swing.JPanel();
        javax.swing.JLabel jLabel3 = new javax.swing.JLabel();
        panelDFG = new javax.swing.JPanel();
        javax.swing.JLabel jLabel2 = new javax.swing.JLabel();
        panel_ASAP_ALAP = new javax.swing.JPanel();
        javax.swing.JLabel jLabel6 = new javax.swing.JLabel();
        panelListSchedule = new javax.swing.JPanel();
        javax.swing.JLabel jLabel5 = new javax.swing.JLabel();
        panelILPSchedule = new javax.swing.JPanel();
        javax.swing.JLabel jLabel8 = new javax.swing.JLabel();
        panelDesignSpaceExploration = new javax.swing.JPanel();

        jLabel7.setText("jLabel7");

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setText("Scheduling Results");

        jLabel4.setText("Input Analytics:");

        panelInputAnalytics.setLayout(new java.awt.GridLayout(1, 1));

        jLabel3.setText("Scheduling Graph (DFG):");

        panelDFG.setLayout(new java.awt.GridLayout(1, 1));

        jLabel2.setText("ASAP/ALAP Schedule Results:");

        panel_ASAP_ALAP.setLayout(new java.awt.GridLayout(1, 2));

        jLabel6.setText("List Schedule Results:");

        panelListSchedule.setLayout(new java.awt.GridLayout(1, 2));

        jLabel5.setText("ILP Solver Schedule Results:");

        panelILPSchedule.setLayout(new java.awt.GridLayout(1, 2));

        jLabel8.setText("Design Space Exploration Results:");

        panelDesignSpaceExploration.setLayout(new java.awt.GridLayout(1, 2));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel_ASAP_ALAP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelInputAnalytics, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelDFG, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelListSchedule, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelILPSchedule, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5)
                            .addComponent(jLabel8))
                        .addGap(0, 207, Short.MAX_VALUE))
                    .addComponent(panelDesignSpaceExploration, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel1)
                .addGap(5, 5, 5)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelInputAnalytics, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelDFG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_ASAP_ALAP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelListSchedule, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelILPSchedule, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelDesignSpaceExploration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel panelDFG;
    private javax.swing.JPanel panelDesignSpaceExploration;
    private javax.swing.JPanel panelILPSchedule;
    private javax.swing.JPanel panelInputAnalytics;
    private javax.swing.JPanel panelListSchedule;
    private javax.swing.JPanel panel_ASAP_ALAP;
    // End of variables declaration//GEN-END:variables
}
