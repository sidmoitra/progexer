/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.parse;

import com.sid.codedesign.progexer.core.parse.ResourceType;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Siddhartha Moitra
 */
public class DefaultParser {

    private static final String STMT_COMMENT_START = "c";
    private static final String STMT_RESOURCE_START = "r";
    private static final String STMT_OPERATION_START = "o";
    private static final String STMT_EDGE_START = "e";

    public ProblemDescription parseProblemDescription(String[] statements) {
        if (statements == null || statements.length == 0) {
            throw new IllegalArgumentException("Cannot parse problem description if there are not statements.");
        }
        final ProblemDescription problemDescription = new ProblemDescription();

        int i = 0;
        for (String statement : statements) {
            i++;
            statement = statement.trim();
            // Exclude comments
            if (statement.startsWith(STMT_COMMENT_START)) {
                continue;
            }

            if (statement.startsWith(STMT_RESOURCE_START)) {
                problemDescription.addResource(parseResource(statement, i));
            } else if (statement.startsWith(STMT_OPERATION_START)) {
                problemDescription.addOperation(parseOperation(statement, i));
            } else if (statement.startsWith(STMT_EDGE_START)) {
                problemDescription.addDependency(parseEdge(statement, problemDescription, i));
            } else {
                throw new IllegalArgumentException("Invalid statement: " + statement + "; at line number: " + i);
            }
        }

        return problemDescription;
    }

    public ProblemDescription parseProblemDescription(File file) {
        System.out.println("Start: File parsing into problem description...");
        if (file == null || !file.canRead()) {
            throw new IllegalArgumentException("File cannot be read: " + file.getAbsolutePath());
        }

        List<String> statementList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                statementList.add(line.trim());
            }
        } catch (IOException ex) {
            System.err.println("Error: While parsing file-"+ex.getMessage());
        }

        return parseProblemDescription(statementList.toArray(new String[0]));
    }

    private Resource parseResource(String statement, int lineNumber) {
        final String STMT_RESOURCE_REGEX = "(r\\s+)([A-Z]+\\s+)(\\d+\\s+)(\\d+\\s+)(\\d+|-)";
        final Pattern pat = Pattern.compile(STMT_RESOURCE_REGEX);

        final Matcher matcher = pat.matcher(statement);
        if (matcher.find()) {
            String type = matcher.group(2).trim().toUpperCase();
            String latency = matcher.group(3).trim();
            String cost = matcher.group(4).trim();
            String allocation = matcher.group(5).trim();

            return new Resource(ResourceType.valueOf(type), Integer.parseInt(latency), Integer.parseInt(cost), (allocation.equals("-") ? -1 : Integer.parseInt(allocation)));
        } else {
            throw new IllegalArgumentException("Resource statement malformed: " + statement + "; at line number: " + lineNumber);
        }
    }

    private Operation parseOperation(String statement, int lineNumber) {
        final String STMT_OPERATION_REGEX = "(o\\s+)([A-Za-z0-9]+\\s+)([A-Z]+\\s*)";
        final Pattern pat = Pattern.compile(STMT_OPERATION_REGEX);

        final Matcher matcher = pat.matcher(statement);
        if (matcher.find()) {
            String name = matcher.group(2).trim();
            String type = matcher.group(3).trim().toUpperCase();

            return new Operation(name, ResourceType.valueOf(type));
        } else {
            throw new IllegalArgumentException("Operation statement malformed: " + statement + "; at line number: " + lineNumber);
        }
    }

    private Dependency parseEdge(String statement, ProblemDescription problemDescription, int lineNumber) {
        final String STMT_EDGE_REGEX = "(e\\s+)([A-Za-z0-9]+\\s+)([A-Za-z0-9]+\\s*)";
        final Pattern pat = Pattern.compile(STMT_EDGE_REGEX);

        final Matcher matcher = pat.matcher(statement);
        if (matcher.find()) {
            String source = matcher.group(2).trim();
            String sink = matcher.group(3).trim();

            Operation sourceOp = problemDescription.getOperationByName(source);
            Operation sinkOp = problemDescription.getOperationByName(sink);
            if (sourceOp == null || sinkOp == null) {
                throw new IllegalArgumentException("Either the source operation or the sink operation for this edge is not defined: " + statement);
            }

            return new Dependency(sourceOp, sinkOp);
        } else {
            throw new IllegalArgumentException("Edge statement malformed: " + statement + "; at line number: " + lineNumber);
        }
    }

}
