/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.parse;

import com.sid.codedesign.progexer.core.parse.ResourceType;
import java.util.Objects;

/**
 * Default implementation of IOperation.
 * 
 * @author Siddhartha Moitra
 */
public class Operation {
    private final String name;
    private final ResourceType type;
    
    public Operation(String name, ResourceType type) {
        this.name = name;
        this.type = type;
    }
    public String getName() {
        return name;
    }
    public ResourceType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Operation other = (Operation) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Operation{" + "name=" + name + ", type=" + type + '}';
    }
}
