/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.parse;

import java.util.HashSet;
import java.util.Set;

/**
 * Default implementation of IProblemDescription.
 *
 * @author Siddhartha Moitra
 */
public class ProblemDescription {

    private Set<Resource> resources;
    private Set<Operation> operations;
    private Set<Dependency> dependencies;

    public ProblemDescription() {
        resources = new HashSet<>();
        operations = new HashSet<>();
        dependencies = new HashSet<>();
    }

    public Set<Operation> getOperations() {
        return operations;
    }

    public Set<Resource> getResources() {
        return resources;
    }

    public Set<Dependency> getDependencies() {
        return dependencies;
    }

    public void setResources(Set<Resource> resources) {
        this.resources = resources;
    }

    public void setOperations(Set<Operation> operations) {
        this.operations = operations;
    }

    public void setDependencies(Set<Dependency> dependencies) {
        this.dependencies = dependencies;
    }

    public void addOperation(Operation operation) {
        if (operation != null && !this.operations.contains(operation)) {
            this.operations.add(operation);
        } else {
            throw new IllegalArgumentException("Operation already defined with name = " + operation.getName());
        }
    }

    public void addResource(Resource resource) {
        if (resource != null && !this.resources.contains(resource)) {
            this.resources.add(resource);
        } else {
            throw new IllegalArgumentException("Resource already defined with type = " + resource.getType().toString());
        }
    }

    public void addDependency(Dependency dependency) {
        if (dependency != null && !this.dependencies.contains(dependency)) {
            this.dependencies.add(dependency);
        } else {
            throw new IllegalArgumentException("Edge already defined with source = " + dependency.getSource().getName() + "; sink = " + dependency.getSink().getName());
        }
    }
    

    /**
     * Returns the operation defined by name. Returns null, if no operation is
     * defined by the specified name.
     *
     * @param name
     * @return
     */
    public Operation getOperationByName(String name) {
        if (operations.size() > 0) {
            for (Operation operation : operations) {
                if (operation.getName().equals(name)) {
                    return operation;
                }
            }
        }
        return null;
    }
    
    public Resource getResourceByType(ResourceType type) {
        if (resources.size() > 0) {
            for(Resource res: resources) {
                if (res.getType() == type) {
                    return res;
                }
            }
        }
        return null;
    }
}
