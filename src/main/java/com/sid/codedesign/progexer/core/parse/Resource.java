/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.parse;

import java.util.Objects;

/**
 * Default implementation of IResource.
 *
 * @author Siddhartha Moitra
 */
public class Resource {

    private final ResourceType type;
    private final int latency;
    private final int cost;
    private final int allocation;

    public Resource(ResourceType type, int latency, int cost, int allocation) {
        this.type = type;
        this.latency = latency;
        this.cost = cost;
        this.allocation = allocation;
    }

    public ResourceType getType() {
        return type;
    }

    public int getLatency() {
        return latency;
    }

    public int getCost() {
        return cost;
    }

    public int getAllocation() {
        return allocation;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Resource other = (Resource) obj;
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Resource{" + "type=" + type + ", latency=" + latency + ", cost=" + cost + ", allocation=" + allocation + '}';
    }
}
