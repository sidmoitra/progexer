/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.schedule;

import com.sid.codedesign.progexer.core.parse.ProblemDescription;
import com.sid.codedesign.progexer.core.schedule.model.OperationEdge;
import java.util.List;

import com.sid.codedesign.progexer.core.schedule.model.OperationNode;

/**
 * @author Siddhartha Moitra
 *
 */
public class ALAPScheduler {

    @SuppressWarnings("unused")
    private final ProblemDescription problemDescription;
    private final OperationNode root;

    private boolean isALAPExecuted;

    public ALAPScheduler(ProblemDescription pd, OperationNode root) {
        if (pd == null) {
            throw new IllegalArgumentException("Problem description cannot be null.");
        }
        if (root == null) {
            throw new IllegalArgumentException("DFG root cannot be null");
        }

        if (root.getOperationStartTime() != 1 || root.getOperationEndTime() != 1) {
            throw new IllegalStateException("I think DFG root has not been executed for ASAP Scheduling before.");
        }

        OperationNode end = SchedulerUtils.getEnd(root);
        if (end.getOperationStartTime() <= 1 || end.getOperationEndTime() <= 1) {
            throw new IllegalStateException("I think DFG root has not been executed for ASAP Scheduling before.");
        }

        this.problemDescription = pd;
        this.root = root;
        this.isALAPExecuted = false;
    }

    private void changeExecutionStatus(List<OperationNode> nodes) {
        for (OperationNode node : nodes) {
            node.setExecutionStatus(ExecutionStatus.FINISHED_EXECUTION);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.sid.codedesign.progexer.core.schedule.IScheduler#executeSchedule(
     * boolean)
     */
    public void executeSchedule(boolean applyResourceConstraint) {
        if (!isALAPExecuted) {
            System.out.println("Start: Executing ALAP Schedule...");
            // Build slack
            /**
             * 1. We traverse nodes in such a way that the child nodes always
             * appears before the parent in the list. This way we pull the child
             * nodes down before their parents
             */
            List<OperationNode> nodeList = SchedulerUtils.getNodesBFS(root);

            // We start from n-1 until 1
            for (int i = nodeList.size() - 1; i >= 0; i--) {
                OperationNode node = nodeList.get(i);
                // We pull the child whose start is latest i.e. min
                int childMinStart = Integer.MAX_VALUE;
                for (OperationEdge nodeEdge : node.getEdges()) {
                    if (nodeEdge.getSink().getOperationStartTime() < childMinStart) {
                        childMinStart = nodeEdge.getSink().getOperationStartTime();
                    }
                }
                // Now the slack will be childMinStart - node.endTime
                if (!SchedulerUtils.isRoot(node) && !SchedulerUtils.isEnd(node)) {
                    ((OperationNode) node).setSlack(childMinStart - node.getOperationEndTime());
                    ((OperationNode) node).setOperationStartTime(
                            node.getOperationStartTime() + node.getSlack());
                    ((OperationNode) node).setOperationEndTime(node.getOperationEndTime() + node.getSlack());
                } else {
                    ((OperationNode) node).setSlack(0);
                }
            }

            changeExecutionStatus(nodeList);
            isALAPExecuted = true;
        } else {
            System.out.println("Warn: ALAP Schedule already executed...");
        }
    }

    public OperationNode getRoot() {
        return this.root;
    }

    public boolean isScheduleExecuted() {
        return isALAPExecuted;
    }

}
