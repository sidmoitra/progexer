/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.schedule;

import com.sid.codedesign.progexer.core.parse.ProblemDescription;
import java.util.List;

import com.sid.codedesign.progexer.core.parse.ResourceType;
import com.sid.codedesign.progexer.core.schedule.model.OperationNode;

/**
 * @author Siddhartha Moitra
 *
 */
public class ASAPScheduler {
    private final ProblemDescription problemDescription;
    
    private final OperationNode root;
    private boolean isASAPExecuted;
    
    public ASAPScheduler(ProblemDescription pd, OperationNode root) {
        if (pd == null)
            throw new IllegalArgumentException("Problem description cannot be null.");
        if (root == null)
            throw new IllegalArgumentException("DFG root cannot be null");
        
        this.problemDescription = pd;
        this.root = root;
        this.isASAPExecuted = false;
    }
    
    private void changeExecutionStatus(List<OperationNode> nodes) {
        for(OperationNode node: nodes) {
            ((OperationNode)node).setExecutionStatus(ExecutionStatus.FINISHED_EXECUTION);
        }
    }
    
    /* (non-Javadoc)
     * @see com.sid.codedesign.progexer.core.schedule.IScheduler#executeSchedule(boolean)
     */
    
    public void executeSchedule(boolean applyResourceConstraint) {
        if (!isASAPExecuted) {
            System.out.println("Start: Executing ASAP Schedule...");
            if (!applyResourceConstraint) {
                final List<OperationNode> exploredNodes = SchedulerUtils.getNodesBFS(root);
                
                // I know the 1st node is NOP-Start and Last is NOP-End
                OperationNode startNode = (OperationNode) exploredNodes.get(0);
                // Validate
                if (startNode.getOperationType() != ResourceType.NOP) {
                    throw new IllegalStateException("Start node must be NOP but it is: " + startNode.getOperationType());
                }

                startNode.setOperationStartTime(1);
                startNode.setOperationEndTime(1);
                // For rest of the nodes
                for (int i = 1; i < exploredNodes.size(); i++) {
                    OperationNode node = (OperationNode) exploredNodes.get(i);
                    // The operation start time will be max of parent(opStartTime + Latency) + 1
                    int startTime = -1;
                    for (OperationNode parent : node.getParents()) {
                        if (startTime < parent.getOperationEndTime()) {
                            startTime = parent.getOperationEndTime();
                        }
                    }
                    
                    node.setOperationStartTime(startTime);
                    node.setOperationEndTime(startTime + SchedulerUtils.getResourceOfType(problemDescription, node.getOperationType()).getLatency());
                }
                
                changeExecutionStatus(exploredNodes);

                /*for(int i = 0; i < exploredNodes.size(); i++) {
                    System.out.println(exploredNodes.get(i));
                }*/
            }
            
            isASAPExecuted = true;
        } else {
            System.out.println("Warn: ASAP Schedule already executed...");
        }
    }

    
    public OperationNode getRoot() {
        return this.root;
    }

    
    public boolean isScheduleExecuted() {
        return isASAPExecuted;
    }

}
