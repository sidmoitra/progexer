/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.schedule;

import com.sid.codedesign.progexer.core.parse.ProblemDescription;
import com.sid.codedesign.progexer.core.parse.Resource;
import com.sid.codedesign.progexer.core.parse.ResourceType;
import com.sid.codedesign.progexer.core.schedule.model.DesignExplorationResult;
import com.sid.codedesign.progexer.core.schedule.model.OperationNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Siddhartha Moitra
 */
public class DesignSpaceExploration {

    private final ProblemDescription problemDescription;
    private final OperationNode root;
    private final int minLatency;
    private final List<DesignExplorationResult> explorationResults;

    public DesignSpaceExploration(ProblemDescription pd, OperationNode root) {
        if (pd == null) {
            throw new IllegalArgumentException("Problem description cannot be null.");
        }
        if (root == null) {
            throw new IllegalArgumentException("DFG root cannot be null");
        }

        if (root.getOperationStartTime() != 1 || root.getOperationEndTime() != 1 || root.getSlack() != 0) {
            throw new IllegalStateException("I think DFG root has not been executed for ALAP Scheduling before.");
        }

        OperationNode end = SchedulerUtils.getEnd(root);
        if (end.getOperationStartTime() <= 1 || end.getOperationEndTime() <= 1 || root.getSlack() != 0) {
            throw new IllegalStateException("I think DFG root has not been executed for ALAP Scheduling before.");
        }

        this.problemDescription = pd;
        this.root = root;
        this.minLatency = SchedulerUtils.getEnd(root).getOperationStartTime() - root.getOperationStartTime();
        this.explorationResults = new ArrayList<>();
    }

    public void executeSchedule() {
        System.out.println("Start: Executing Design Space Exploration using List Scheduling...");
        // 1. Select a type of resource and increase its allocation until the current
        // 2. Other resources hold at 1
        // This is same for all
        Resource resource_NOP = problemDescription.getResourceByType(ResourceType.NOP);
        for (Resource resource : problemDescription.getResources()) {
            if (resource.getType() == ResourceType.NOP) {
                continue;
            }

            for (int i = 1; i <= resource.getAllocation(); i++) {
                ProblemDescription newPD = new ProblemDescription();
                newPD.setDependencies(problemDescription.getDependencies());
                newPD.setOperations(problemDescription.getOperations());
                newPD.addResource(resource_NOP);
                for (Resource otherResource : problemDescription.getResources()) {
                    if (otherResource.getType() != ResourceType.NOP && otherResource.getType() != resource.getType()) {
                        newPD.addResource(new Resource(otherResource.getType(),
                                otherResource.getLatency(), otherResource.getCost(),
                                1));
                    }
                }
                newPD.addResource(new Resource(resource.getType(), resource.getLatency(), resource.getCost(), i));
                // Perform a ListSchedule with this and record the results
                OperationNode clonedRoot = SchedulerUtils.cloneTree(root);
                ListScheduler ls = new ListScheduler(newPD, clonedRoot);
                ls.executeSchedule(true);
                // If this resource allocation gives latency less than ALAP
                // then increasing the resource will atleast give this much
                // So we break
                int latency = SchedulerUtils.getEnd(clonedRoot).getOperationStartTime() - clonedRoot.getOperationStartTime();
                if (latency < minLatency) {
                    break;
                }
                // We store this
                this.explorationResults.add(new DesignExplorationResult(newPD.getResources(), clonedRoot));
            }
        }
    }

    public List<DesignExplorationResult> getExplorationResults() {
        return Collections.unmodifiableList(explorationResults);
    }
}
