/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.schedule;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.sid.codedesign.progexer.core.parse.Dependency;
import com.sid.codedesign.progexer.core.parse.Operation;
import com.sid.codedesign.progexer.core.parse.ProblemDescription;
import com.sid.codedesign.progexer.core.parse.Resource;
import com.sid.codedesign.progexer.core.parse.ResourceType;
import com.sid.codedesign.progexer.core.schedule.model.OperationNode;

import net.sf.javailp.Linear;
import net.sf.javailp.Operator;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryLpSolve;

/**
 * @author Siddhartha Moitra
 *
 */
public class ILPScheduler {

    public static final String VAR_SEPARATOR = "_";
    public static final String COMMENT_START = "/********************************************************/";
    public static final String COMMENT_END = "/********************************************************/";
    public static final String NEWLINE = "";
    public static final String STATEMENT_END = ";";
    public static final String SPACE = " ";
    public static final String OP_LE = "<=";
    public static final String OP_GE = ">=";
    public static final String OP_EQ = "=";
    public static final String OP_ADD = "+";

    protected final ProblemDescription problemDescription;
    protected final List<OperationNode> nodes;

    protected final int latencyBound;
    protected final List<String> ops;

    protected boolean isILPScheduleExecuted;

    public ILPScheduler(ProblemDescription pd, OperationNode root) {
        if (pd == null) {
            throw new IllegalArgumentException("Problem description cannot be null.");
        }
        if (root == null) {
            throw new IllegalArgumentException("DFG root cannot be null");
        }

        if (root.getOperationStartTime() != 1 || root.getOperationEndTime() != 1) {
            throw new IllegalStateException("I think DFG root has not been executed for List Scheduling before.");
        }

        OperationNode end = SchedulerUtils.getEnd(root);
        if (end.getOperationStartTime() <= 1 || end.getOperationEndTime() <= 1) {
            throw new IllegalStateException("I think DFG root has not been executed for List Scheduling before.");
        }

        this.problemDescription = pd;
        this.isILPScheduleExecuted = false;

        latencyBound = end.getOperationStartTime() - root.getOperationStartTime();
        this.nodes = SchedulerUtils.getNodesBFS(root);
        Collections.sort(nodes, new OperationNameComparator());

        ops = new ArrayList<>(nodes.size() - 1);
        // I know the 1st node is start
        for (int i = 1; i < nodes.size(); i++) {
            ops.add(nodes.get(i).getOperationName());
        }
    }

    private void addMultilineComments(List<String> statements, String comment) {
        statements.add(NEWLINE);
        statements.add(COMMENT_START);
        statements.add("/* " + comment + " */");
        statements.add(COMMENT_END);
    }

    private void addSinglelineComments(List<String> statements, String comment) {
        statements.add(NEWLINE);
        statements.add("/* " + comment + " */");
    }

    private void defineObjectiveFunction(List<String> statements, Problem problem) {
        addMultilineComments(statements, "objective function");

        // For nop2
        Operation endOp = null;
        for (Dependency edge : problemDescription.getDependencies()) {
            if (edge.getSink().getType() == ResourceType.NOP) {
                endOp = edge.getSink();
                break;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("min: ");
        Linear objectiveFn = new Linear();
        // for each time step
        for (int timeStep = 1; timeStep <= latencyBound; timeStep++) {
            String opVar = getVariableNameFor(endOp.getName(), timeStep);
            // Add to linear
            objectiveFn.add(timeStep, opVar);
            // Add to statements
            sb.append(timeStep).append(SPACE).append(opVar).append(SPACE).append(OP_ADD).append(SPACE);
        }

        problem.setObjective(objectiveFn, OptType.MIN);

        // Remove the last SPACE and ADD
        sb.replace(sb.length() - 3, sb.length(), "");
        sb.append(STATEMENT_END);

        statements.add(sb.toString());
    }

    private void defineResourceConstraints(List<String> statements, Problem problem) {
        addMultilineComments(statements, "resource constraint");

        // For each time step
        for (int timeStep = 1; timeStep <= latencyBound; timeStep++) {
            // For every resource except NOP
            for (Resource resource : problemDescription.getResources()) {
                if (resource.getType() != ResourceType.NOP) {
                    Linear resourceConstraints = new Linear();

                    StringBuilder sb = new StringBuilder();
                    List<Operation> ops = getOperationsForResourceType(resource.getType());
                    for (Operation op : ops) {
                        String varName = getVariableNameFor(op.getName(), timeStep);
                        // Add to linear
                        resourceConstraints.add(1, varName);
                        // Add to statements
                        sb.append(varName).append(SPACE).append(OP_ADD).append(SPACE);
                        // Here we have an extra condition:
                        // If Resource Latency > 1
                        // For this case we also check if the op started in
                        // previous timesteps
                        // but executing in this time step also
                        if (resource.getLatency() > 1) {
                            for (Integer possibleTimestamp : getPossiblePreviousTimestampsForOp(op, resource,
                                    timeStep)) {
                                String previousVarName = getVariableNameFor(op.getName(), possibleTimestamp);
                                // Add to linear
                                resourceConstraints.add(1, previousVarName);
                                // Add to statements
                                sb.append(previousVarName).append(SPACE).append(OP_ADD).append(SPACE);
                            }
                        }
                    }
                    problem.add(resourceConstraints, Operator.LE, resource.getAllocation());

                    // Remove the last SPACE and ADD
                    if (sb.length() > 3) {
                        sb.replace(sb.length() - 3, sb.length(), SPACE);
                        sb.append(OP_LE).append(SPACE).append(resource.getAllocation()).append(STATEMENT_END);
                        addSinglelineComments(statements, resource.getType() + " @ ts:" + timeStep);
                        statements.add(sb.toString());
                    }
                }
            }
            statements.add(NEWLINE);
        }
    }

    private List<Integer> getPossiblePreviousTimestampsForOp(Operation op, Resource resource, int currentTimestamp) {
        List<Integer> possibleTimestamps = new ArrayList<>();
        // I know currentTimestamp is added before calling this function
        --currentTimestamp;
        int latency = resource.getLatency() - 1;
        while (currentTimestamp > 0 && latency > 0) {
            possibleTimestamps.add(currentTimestamp);
            --currentTimestamp;
            --latency;
        }
        return possibleTimestamps;
    }

    private void defineSequencingConstraints(List<String> statements, Problem problem) {
        addMultilineComments(statements, "sequencing constraints");

        // For every edge
        for (Dependency edge : problemDescription.getDependencies()) {
            if (edge.getSource().getType() != ResourceType.NOP) {
                statements.add("/* " + edge.getSource().getName() + " -> " + edge.getSink().getName() + " */");

                // Create linear equation for source and sink
                StringBuilder finalEq = new StringBuilder();
                StringBuilder sbSource = new StringBuilder();
                StringBuilder sbSink = new StringBuilder();

                Linear seqConstraints = new Linear();

                // for each time step
                for (int timeStep = 1; timeStep <= latencyBound; timeStep++) {
                    String sourceVar = getVariableNameFor(edge.getSource().getName(), timeStep);
                    String sinkVar = getVariableNameFor(edge.getSink().getName(), timeStep);

                    seqConstraints.add(timeStep, sinkVar);
                    seqConstraints.add((-1 * timeStep), sourceVar);

                    sbSource.append(timeStep).append(SPACE).append(sourceVar).append(SPACE).append(OP_ADD)
                            .append(SPACE);
                    sbSink.append(timeStep).append(SPACE).append(sinkVar).append(SPACE).append(OP_ADD).append(SPACE);
                }

                problem.add(seqConstraints, Operator.GE, 1);

                // Remove the last SPACE and ADD
                sbSource.replace(sbSource.length() - 3, sbSource.length(), "");
                sbSink.replace(sbSink.length() - 3, sbSink.length(), SPACE);
                // Complete equation
                finalEq.append(sbSink).append(SPACE).append(OP_GE).append(SPACE).append("1").append(SPACE)
                        .append(OP_ADD).append(SPACE).append(sbSource).append(STATEMENT_END);
                statements.add(finalEq.toString());

                statements.add(NEWLINE);
            }
        }
    }

    private void defineUniquenessConstraints(List<String> statements, Problem problem) {
        addMultilineComments(statements, "uniqueness constraints");

        for (String op : ops) {
            Linear constraintForOp = new Linear();
            StringBuilder sb = new StringBuilder();
            for (int timeStep = 1; timeStep <= latencyBound; timeStep++) {
                String varName = getVariableNameFor(op, timeStep);
                // Add to linear
                constraintForOp.add(1, varName);
                // Add to statements
                if (timeStep == latencyBound) {
                    sb.append(varName).append(SPACE).append(OP_EQ).append(SPACE).append("1").append(STATEMENT_END);
                } else {
                    sb.append(varName).append(SPACE).append(OP_ADD).append(SPACE);
                }
            }

            problem.add(constraintForOp, Operator.EQ, 1);

            statements.add(sb.toString());
        }
    }

    private void defineVariables(List<String> statements, Problem problem) {
        addMultilineComments(statements, "integer variables");

        for (String op : ops) {
            for (int timeStep = 1; timeStep <= latencyBound; timeStep++) {
                String varName = getVariableNameFor(op, timeStep);
                // Define variable upper bound
                problem.setVarType(varName, Integer.class);
                problem.setVarUpperBound(varName, 1);

                statements.add("bin " + varName + STATEMENT_END);
            }
        }
    }

    private List<Operation> getOperationsForResourceType(ResourceType type) {
        List<Operation> ops = new ArrayList<>();
        for (Operation op : problemDescription.getOperations()) {
            if (op.getType() == type) {
                ops.add(op);
            }
        }
        return ops;
    }

    private String getVariableNameFor(String op, int timeStep) {
        StringBuilder sb = new StringBuilder();
        sb.append(op).append(VAR_SEPARATOR).append(timeStep);
        return sb.toString();
    }

    private void setOperationStartEndTime(Result result, OperationNode op) {
        op.setOperationStartTime(-1);
        op.setOperationEndTime(-1);
        for (int timeStep = 1; timeStep <= latencyBound; timeStep++) {
            String varName = getVariableNameFor(op.getOperationName(), timeStep);
            Number num = result.getPrimalValue(varName);
            if (num.intValue() == 1) {
                // Operation starts at this timestep
                op.setOperationStartTime(timeStep);
                // get resource executing this op
                Resource resource = problemDescription.getResourceByType(op.getOperationType());
                op.setOperationEndTime(op.getOperationStartTime() + resource.getLatency());
            }
        }
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sid.codedesign.progexer.core.schedule.IScheduler#executeSchedule(
	 * boolean)
     */
    public void executeSchedule(boolean applyResourceConstraint) {
        if (!isILPScheduleExecuted) {
            System.out.println("Start: Executing Schedule using ILP Solver...");
            if (applyResourceConstraint) {
                // Define a problem
                Problem problem = new Problem();
                List<String> statements = new ArrayList<>();

                // defineVariablesUpperBound(statements);
                defineObjectiveFunction(statements, problem);
                defineUniquenessConstraints(statements, problem);
                defineResourceConstraints(statements, problem);
                defineSequencingConstraints(statements, problem);
                defineVariables(statements, problem);

                // Write statements to a temp file
                String fileName = SchedulerUtils.USR_TMP_DIR + File.separator + SchedulerUtils.FILE_PREFIX
                        + System.currentTimeMillis() + SchedulerUtils.FILE_EXT;
                File file = SchedulerUtils.writeToFile(fileName, statements);
                System.out.println("Successfully written file for LP_SOLVER at location: " + file.getAbsolutePath());

                // Solve the problem
                SolverFactory factory = new SolverFactoryLpSolve(); // use
                // lp_solve
                factory.setParameter(Solver.VERBOSE, 0);
                factory.setParameter(Solver.TIMEOUT, 100); // set timeout to 100
                // seconds
                Solver solver = factory.get(); // you should use this solver
                // only once for one problem
                Result result = solver.solve(problem);
                if (result != null) {
                    // I must now get the values of all variables
                    for (OperationNode op : nodes) {
                        // except root and end node
                        if (op.equals(nodes.get(0)) || op.equals(nodes.get(nodes.size() - 1))) {
                            continue;
                        }
                        setOperationStartEndTime(result, op);
                    }
                    // Once done set root start-end time
                    nodes.get(0).setOperationStartTime(1);
                    nodes.get(0).setOperationEndTime(1);
                    // Once done set end start-end time
                    // for this we need value of Optimizing function
                    int objective = result.getObjective().intValue();
                    nodes.get(nodes.size() - 1).setOperationStartTime(objective);
                    nodes.get(nodes.size() - 1).setOperationEndTime(objective);
                } else {
                    System.err.println("ILP Solver could not solve the problem.");
                }

            }
            isILPScheduleExecuted = true;
        } else {
            System.out.println("Warn: Schedule using ILP Solver already executed...");
        }
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see com.sid.codedesign.progexer.core.schedule.IScheduler#getRoot()
     */
    public OperationNode getRoot() {
        return this.nodes.get(0);
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sid.codedesign.progexer.core.schedule.IScheduler#isScheduleExecuted()
     */
    public boolean isScheduleExecuted() {
        return isILPScheduleExecuted;
    }

}
