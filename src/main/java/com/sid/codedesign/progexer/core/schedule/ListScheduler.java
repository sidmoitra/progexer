/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.schedule;

import com.sid.codedesign.progexer.core.parse.ProblemDescription;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import com.sid.codedesign.progexer.core.schedule.model.OperationEdge;
import com.sid.codedesign.progexer.core.schedule.model.OperationNode;

/**
 * @author Siddhartha Moitra
 *
 */
public class ListScheduler {
    private ResourceStore resourceStore;
    private final ProblemDescription problemDescription;
    private final OperationNode root;

    private boolean isListScheduleExecuted;

    public ListScheduler(ProblemDescription pd, OperationNode root) {
        if (pd == null)
            throw new IllegalArgumentException("Problem description cannot be null.");
        if (root == null)
            throw new IllegalArgumentException("DFG root cannot be null");

        if (root.getOperationStartTime() != 1 || root.getOperationEndTime() != 1 || root.getSlack() != 0)
            throw new IllegalStateException("I think DFG root has not been executed for ALAP Scheduling before.");

        OperationNode end = SchedulerUtils.getEnd(root);
        if (end.getOperationStartTime() <= 1 || end.getOperationEndTime() <= 1 || root.getSlack() != 0)
            throw new IllegalStateException("I think DFG root has not been executed for ALAP Scheduling before.");

        this.problemDescription = pd;
        this.root = root;
        this.isListScheduleExecuted = false;

        resourceStore = new ResourceStore(pd.getResources(), 0);
        
        // Make sure root and end have max slacks
        /*((OperationNode)this.root).setSlack(Integer.MAX_VALUE);
        ((OperationNode)end).setSlack(Integer.MAX_VALUE);*/
    }

    public void executeSchedule(boolean applyResourceConstraint) {
        if (!isListScheduleExecuted) {
            System.out.println("Start: Executing List Schedule...");
            if (applyResourceConstraint) {
                final SlackComaparator sc = new SlackComaparator();
                
                // We define 1 DS, for executing/executed
                List<OperationNode> closed = new ArrayList<>();
                List<OperationNode> ready = new ArrayList<>();
                OperationNode current = this.root;
                ((OperationNode) current).setOperationStartTime(1);
                ((OperationNode) current).setOperationEndTime(1);
                ((OperationNode) current).setExecutionStatus(ExecutionStatus.FINISHED_EXECUTION);   
                closed.add(current);
                
                expandNode(current, closed, ready);
                
                int clockCycle = 0;
                while (closed.size() < problemDescription.getOperations().size()) {
                    ++clockCycle;
                    List<OperationNode> releasedNodes = resourceStore.incrementClock();
                    if (releasedNodes.size() > 0) {
                        processExecutingNodes(closed, releasedNodes, clockCycle);
                        for(OperationNode nodeToExpand: releasedNodes) {
                            expandNode(nodeToExpand, closed, ready);
                        }
                    }
                    // Sort ready based on slack
                    Collections.sort(ready, sc);
                    
                    ListIterator<OperationNode> iter = ready.listIterator();
                    while (iter.hasNext()) {
                        OperationNode toBeExecuted = iter.next();
                        if (toBeExecuted.getExecutionStatus() == ExecutionStatus.NOT_STARTED) {
                            if (SchedulerUtils.isEnd(toBeExecuted)) {
                                ((OperationNode) toBeExecuted).setExecutionStatus(ExecutionStatus.FINISHED_EXECUTION);
                                ((OperationNode) toBeExecuted).setOperationStartTime(clockCycle);
                                ((OperationNode) toBeExecuted).setOperationEndTime(clockCycle);
                                closed.add(toBeExecuted);
                                iter.remove();
                            }
                            if (resourceStore.acquireResource(toBeExecuted)) {
                                // 1. Set execution status
                                ((OperationNode) toBeExecuted).setExecutionStatus(ExecutionStatus.EXECUTING);
                                ((OperationNode) toBeExecuted).setOperationStartTime(clockCycle);
                                iter.remove();
                            }
                        }
                    } // End of while(iter.hasNext)
                }
            } // End of if(applyResourceConstraints)
            isListScheduleExecuted = true;
        } else {
            System.out.println("Warn: List Schedule already executed...");
        }
    }
    
    private void expandNode(OperationNode nodeToExpand, List<OperationNode> closed, List<OperationNode> ready) {
        //While expanding a node, only take those children whose all parents are in closed
        for(OperationEdge edge: nodeToExpand.getEdges()) {
            OperationNode child = edge.getSink();
            boolean isAllParentClosed = true;
            for (OperationNode parent: child.getParents()) {
                if (!closed.contains(parent)) {
                    isAllParentClosed = false;
                    break;
                }
            }
            if (isAllParentClosed) {
                ready.add(child);
            }
        }
    }

    private void processExecutingNodes(List<OperationNode> closed, List<OperationNode> releasedNodes, int clockCycle) {
        ListIterator<OperationNode> iter = releasedNodes.listIterator();
        while (iter.hasNext()) {
            OperationNode releasedNode = iter.next();
            ((OperationNode) releasedNode).setOperationEndTime(clockCycle);
            ((OperationNode) releasedNode).setExecutionStatus(ExecutionStatus.FINISHED_EXECUTION);
            closed.add(releasedNode);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.sid.codedesign.progexer.core.schedule.IScheduler#getRoot()
     */
    
    public OperationNode getRoot() {
        return root;
    }

    
    public boolean isScheduleExecuted() {
        return isListScheduleExecuted;
    }

    public ProblemDescription getProblemDescription() {
        return problemDescription;
    }
}
