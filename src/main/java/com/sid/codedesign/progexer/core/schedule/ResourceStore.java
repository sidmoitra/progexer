/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.schedule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import com.sid.codedesign.progexer.core.parse.ResourceType;
import com.sid.codedesign.progexer.core.schedule.model.OperationNode;

/**
 * @author Siddhartha Moitra
 *
 */
public class ResourceStore {

    private class Resource {

        boolean isLocked;
        OperationNode operatingNode;
        int lockedAtClock;
        final int latency;

        private Resource(com.sid.codedesign.progexer.core.parse.Resource res) {
            latency = res.getLatency();
            this.operatingNode = null;
            this.lockedAtClock = -1;
            this.isLocked = false;
        }

        private void lock(OperationNode node, int time) {
            if (isLocked) {
                throw new IllegalStateException("Cannot lock a resource if it is already locked.");
            }

            this.operatingNode = node;
            this.lockedAtClock = time;
            this.isLocked = true;
        }

        private void release() {
            this.operatingNode = null;
            this.lockedAtClock = -1;
            this.isLocked = false;
        }
    }

    private int currentTime;
    private EnumMap<ResourceType, List<Resource>> resourceMap;
    private List<Resource> lockedResources;

    public ResourceStore(Set<com.sid.codedesign.progexer.core.parse.Resource> resources, int startClockCycleAt) {
        currentTime = startClockCycleAt;
        lockedResources = new ArrayList<>();

        resourceMap = new EnumMap<>(ResourceType.class);
        for (com.sid.codedesign.progexer.core.parse.Resource resource : resources) {
            if (!resourceMap.containsKey(resource.getType())) {
                List<Resource> resourceList = new ArrayList<>();
                for (int i = 0; i < resource.getAllocation(); i++) {
                    resourceList.add(new Resource(resource));
                }
                resourceMap.put(resource.getType(), Collections.unmodifiableList(resourceList));
            }
        }
    }

    public List<OperationNode> incrementClock() {
        currentTime++;
        return releaseResources();
    }

    public boolean acquireResource(OperationNode node) {
        Resource availableResource = hasAvailableResource(resourceMap.get(node.getOperationType()));
        if (availableResource == null) {
            return false;
        }

        availableResource.lock(node, currentTime);
        lockedResources.add(availableResource);
        return true;
    }

    private List<OperationNode> releaseResources() {
        List<OperationNode> removedNodes = new ArrayList<>();
        ListIterator<Resource> iter = lockedResources.listIterator();
        while (iter.hasNext()) {
            Resource lockedResource = iter.next();
            if (currentTime == lockedResource.lockedAtClock + lockedResource.latency) {
                removedNodes.add(lockedResource.operatingNode);
                lockedResource.release();
                iter.remove();
            }
        }
        return removedNodes;
    }

    private Resource hasAvailableResource(List<Resource> resources) {
        for (Resource resource : resources) {
            if (!resource.isLocked) {
                return resource;
            }
        }

        return null;
    }
}
