/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.schedule;

import com.sid.codedesign.progexer.core.parse.Dependency;
import com.sid.codedesign.progexer.core.parse.Operation;
import com.sid.codedesign.progexer.core.parse.ProblemDescription;
import com.sid.codedesign.progexer.core.parse.Resource;
import com.sid.codedesign.progexer.core.parse.ResourceType;
import com.sid.codedesign.progexer.core.schedule.model.DesignExplorationResult;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import com.sid.codedesign.progexer.core.schedule.model.OperationEdge;
import com.sid.codedesign.progexer.core.schedule.model.OperationNode;

/**
 * @author Siddhartha Moitra
 *
 */
public class SchedulerUtils {

    public static final String USR_TMP_DIR = System.getProperty("java.io.tmpdir");
    public static final String FILE_PREFIX = "lp_solve_file_";
    public static final String FILE_EXT = ".lp";

    public static File writeToFile(String fileName, List<String> statements) {
        File file = new File(fileName);
        try {
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            } else {
                // Delete and create new
                file.delete();
                file.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(file);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            for (String statement : statements) {
                bw.write(statement);
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {

        }
        return file;
    }

    public static OperationNode cloneTree(OperationNode root) {
        if (root == null) {
            return null;
        }
        if (!root.getParents().isEmpty()) {
            throw new IllegalStateException("Cannot clone a tree if node is not root.");
        }
        if (root.getOperationType() != ResourceType.NOP) {
            throw new IllegalStateException("Cannot clone a tree if root node is not of type NOP.");
        }

        LinkedList<OperationNode> queue = new LinkedList<OperationNode>();
        Map<String, OperationNode> map = new HashMap<>();

        OperationNode clonedRoot = new OperationNode(root);

        queue.add(root);
        map.put(clonedRoot.getOperationName(), clonedRoot);

        while (!queue.isEmpty()) {
            OperationNode curr = queue.pop();
            List<OperationNode> currNeighbors = new ArrayList<>();
            for (OperationEdge edge : curr.getEdges()) {
                currNeighbors.add(edge.getSink());
            }

            for (OperationNode aNeighbor : currNeighbors) {
                if (!map.containsKey(aNeighbor.getOperationName())) {
                    OperationNode copy = new OperationNode(aNeighbor);
                    map.put(copy.getOperationName(), copy);
                    OperationEdge edge = new OperationEdge();
                    edge.setSink(copy);
                    OperationNode clonedParent = map.get(curr.getOperationName());
                    ((OperationNode) clonedParent).addEdge(edge);
                    ((OperationNode) copy).addParent(clonedParent);

                    queue.add(aNeighbor);
                } else {
                    OperationNode copy = map.get(aNeighbor.getOperationName());
                    OperationEdge edge = new OperationEdge();
                    edge.setSink(copy);
                    OperationNode clonedParent = map.get(curr.getOperationName());
                    ((OperationNode) clonedParent).addEdge(edge);
                    ((OperationNode) copy).addParent(clonedParent);
                }
            }

        }
        return clonedRoot;
    }

    public static OperationNode getEnd(OperationNode node) {
        while (!isEnd(node)) {
            node = node.getEdges().iterator().next().getSink();
        }
        return node;
    }

    public static List<OperationNode> getNodesBFS(OperationNode root) {
        // Datastructures to be used
        Queue<OperationNode> queue = new LinkedList<>();
        List<OperationNode> explored = new ArrayList<>();

        queue.add(root);

        while (!queue.isEmpty()) {
            OperationNode current = queue.poll();
            explored.add(current);

            for (OperationEdge treeEdge : current.getEdges()) {
                OperationNode child = treeEdge.getSink();
                // Add the child to the queue only and only if all its parents
                // are added
                boolean isEveryParentAdded = true;
                for (OperationNode parent : child.getParents()) {
                    if (!parent.equals(current)) {
                        // If any parent is not added , set flag to false
                        if (!explored.contains(parent)) {
                            isEveryParentAdded = false;
                            break;
                        }
                    }
                }

                if (isEveryParentAdded && !queue.contains(child)) {
                    queue.add(child);
                }
            }
        }
        return explored;
    }

    public static List<OperationNode> getNodesReverseBFS(OperationNode node) {
        // Datastructures to be used
        Queue<OperationNode> queue = new LinkedList<>();
        List<OperationNode> explored = new ArrayList<>();

        if (!isEnd(node)) {
            return Collections.unmodifiableList(explored);
        }

        queue.add(node);

        while (!queue.isEmpty()) {
            OperationNode current = queue.poll();
            if (!explored.contains(current)) {
                // Put in list only when all of its children are in the list
                boolean isEveryChildInList = true;
                for (OperationEdge treeEdge : current.getEdges()) {
                    if (!explored.contains(treeEdge.getSink())) {
                        isEveryChildInList = false;
                        break;
                    }
                }
                if (isEveryChildInList) {
                    explored.add(current);
                    for (OperationNode parent : node.getParents()) {
                        queue.add(parent);
                    }
                }
            }
        }

        return Collections.unmodifiableList(explored);
    }

    public static Resource getResourceOfType(ProblemDescription pd, ResourceType type) {
        for (Resource res : pd.getResources()) {
            if (res.getType() == type) {
                return res;
            }
        }
        throw new IllegalArgumentException("No resource defined of type: " + type);
    }

    public static boolean isRoot(OperationNode node) {
        return (node.getOperationType() == ResourceType.NOP && node.getEdges().size() != 0);
    }

    public static boolean isEnd(OperationNode node) {
        return (node.getOperationType() == ResourceType.NOP && node.getEdges().size() == 0);
    }

    public static OperationNode createTree(Set<Dependency> dataDependencies) {
        OperationNode root = null;
        OperationNode end = null;

        final Map<String, OperationNode> nodeMap = new HashMap<>();
        for (Dependency dataDependency : dataDependencies) {

            Operation sourceOp = dataDependency.getSource();
            Operation sinkOp = dataDependency.getSink();

            if (nodeMap.containsKey(sourceOp.getName())) {
                // The source already exists in the tree
                OperationNode sourceNode = nodeMap.get(sourceOp.getName());
                OperationEdge treeEdge = new OperationEdge();
                // Check if sink exists
                if (nodeMap.containsKey(sinkOp.getName())) {
                    OperationNode sinkNode = nodeMap.get(sinkOp.getName());
                    sinkNode.addParent(sourceNode);
                    treeEdge.setSink(sinkNode);
                } else {
                    // Sink does not exist, so we create one
                    OperationNode sinkNode = new OperationNode(sinkOp);
                    sinkNode.addParent(sourceNode);
                    treeEdge.setSink(sinkNode);
                    // Add this new node to map
                    nodeMap.put(sinkNode.getOperationName(), sinkNode);
                }
                sourceNode.addEdge(treeEdge);
            } else {
                // Source node does not exist
                // we create a new source node and attach it to the root
                OperationNode sourceNode = new OperationNode(sourceOp);
                // Add this new node to map
                nodeMap.put(sourceNode.getOperationName(), sourceNode);
                OperationEdge treeEdge = new OperationEdge();
                // Check if sink exists
                if (nodeMap.containsKey(sinkOp.getName())) {
                    OperationNode sinkNode = (OperationNode) nodeMap.get(sinkOp.getName());
                    sinkNode.addParent(sourceNode);
                    treeEdge.setSink(sinkNode);
                } else {
                    // Sink does not exist, so we create one
                    OperationNode sinkNode = new OperationNode(sinkOp);
                    sinkNode.addParent(sourceNode);
                    treeEdge.setSink(sinkNode);
                    // Add this new node to map
                    nodeMap.put(sinkNode.getOperationName(), sinkNode);
                }
                sourceNode.addEdge(treeEdge);
            }
        }

        // We select any node from this map and
        // traverse to find the root and tree sink.
        // Both root and sink must be unique and of type NOP
        Iterator<String> mapIterator = nodeMap.keySet().iterator();
        while (mapIterator.hasNext()) {
            String key = mapIterator.next();
            OperationNode currentNode = nodeMap.get(key);
            if (currentNode.getOperationType() == ResourceType.NOP && currentNode.getParents().isEmpty()) {
                // This is root
                root = currentNode;
            } else if (currentNode.getOperationType() == ResourceType.NOP && currentNode.getEdges().isEmpty()) {
                // This is sink
                end = currentNode;
            }
        }
        if (root == null) {
            throw new RuntimeException("Create scheduler tree validation failed: Root cannot be found.");
        }
        if (end == null) {
            throw new RuntimeException("Create scheduler tree validation failed: Sink cannot be found.");
        }

        return root;
    }
    
    public static void calculateParetoOptimal(List<DesignExplorationResult> explorationResults) {
        // For every point check few conditions
        for(DesignExplorationResult result: explorationResults) {
            if (result.isIsParetoCalComplete())
                continue;
            for(DesignExplorationResult tempRes: explorationResults) {
                if(tempRes.equals(result) || tempRes.isIsParetoCalComplete())
                    continue;
                // Condition 1:
                if (result.getCost() < tempRes.getCost() && result.getLatency() < tempRes.getLatency()) {
                    result.setIsParetoOptimal(true);
                    tempRes.setIsParetoOptimal(false);
                } else if (result.getCost() < tempRes.getCost() && result.getLatency() == tempRes.getLatency()) {
                    result.setIsParetoOptimal(true);
                    tempRes.setIsParetoOptimal(false);
                } else if (result.getCost() == tempRes.getCost() && result.getLatency() < tempRes.getLatency()) {
                    result.setIsParetoOptimal(true);
                    tempRes.setIsParetoOptimal(false);
                }
            }           
        }
    }
}