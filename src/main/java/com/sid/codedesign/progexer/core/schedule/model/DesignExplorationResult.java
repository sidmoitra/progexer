/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.schedule.model;

import com.sid.codedesign.progexer.core.parse.Resource;
import com.sid.codedesign.progexer.core.parse.ResourceType;
import com.sid.codedesign.progexer.core.schedule.SchedulerUtils;
import java.util.Set;

/**
 *
 * @author Siddhartha Moitra
 */
public class DesignExplorationResult {
    private static int counter = 0;
    private final int ID;
    private final Set<Resource> resources;
    private final OperationNode root;
    private final int cost;
    private final int latency;
    private boolean isParetoOptimal;
    
    private boolean isParetoCalComplete;
    
    public DesignExplorationResult(Set<Resource> resources, OperationNode root) {
        ID = ++counter;
        this.resources = resources;
        this.root = root;
        this.latency = SchedulerUtils.getEnd(root).getOperationStartTime() - root.getOperationStartTime();
        
        int cost = 0;
        for (Resource res: resources) {
            if (res.getType() == ResourceType.NOP)
                continue;
            
            cost = cost + (res.getAllocation() * res.getCost());
        }
        this.cost = cost;
        this.isParetoOptimal = true;
        this.isParetoCalComplete = false;
    }

    public boolean isIsParetoCalComplete() {
        return isParetoCalComplete;
    }

    public boolean isIsParetoOptimal() {
        return isParetoOptimal;
    }

    public void setIsParetoOptimal(boolean isParetoOptimal) {
        this.isParetoCalComplete = true;
        this.isParetoOptimal = isParetoOptimal;
    }

    public Set<Resource> getResources() {
        return resources;
    }

    public OperationNode getRoot() {
        return root;
    }
    
    public int getLatency() {
        return latency;
    }
    
    public int getCost() {
        return cost;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.ID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DesignExplorationResult other = (DesignExplorationResult) obj;
        if (this.ID != other.ID) {
            return false;
        }
        return true;
    }
}
