/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.schedule.model;

import com.sid.codedesign.progexer.core.parse.Operation;
import com.sid.codedesign.progexer.core.parse.ResourceType;
import com.sid.codedesign.progexer.core.schedule.ExecutionStatus;
import java.util.HashSet;
import java.util.Set;



/**
 *
 * @author Siddhartha Moitra
 */
public class OperationNode {

	private final String operationName;
	private final ResourceType operationType;
	private Set<OperationNode> parents;
	private Set<OperationEdge> edges;

	private int operationStartTime;
	private int operationEndTime;
	private int slack;
	
	private ExecutionStatus executionStatus;
	
	public OperationNode(OperationNode node) {
		this.operationName = node.getOperationName();
		this.operationType = node.getOperationType();
		this.operationStartTime = node.getOperationStartTime();
		this.operationEndTime = node.getOperationEndTime();
		this.slack = node.getSlack();
		this.executionStatus = ExecutionStatus.NOT_STARTED;
		this.parents = new HashSet<>();
		this.edges = new HashSet<>();
	}
	
	public OperationNode(Operation op) {
		if (op == null)
			throw new IllegalArgumentException("Operation cannot be null.");

		this.operationName = op.getName();
		this.operationType = op.getType();

		this.parents = new HashSet<>();
		this.edges = new HashSet<>();

		this.operationStartTime = -1;
		this.operationEndTime = -1;
		this.slack = -1;
		
		this.executionStatus = ExecutionStatus.NOT_STARTED;
	}

	public void addEdge(OperationEdge edge) {
		if (edge != null)
			this.edges.add(edge);
	}

	public void addParent(OperationNode parent) {
		if (parent != null)
			this.parents.add(parent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof OperationNode)) {
			return false;
		}
		OperationNode other = (OperationNode) obj;
		if (operationName == null) {
			if (other.operationName != null) {
				return false;
			}
		} else if (!operationName.equals(other.operationName)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the edges
	 */
	public Set<OperationEdge> getEdges() {
		return edges;
	}

	public ExecutionStatus getExecutionStatus() {
		return this.executionStatus;
	}

	/**
	 * @return the operationEndTime
	 */
	public int getOperationEndTime() {
		return operationEndTime;
	}

	/**
	 * @return the operationName
	 */
	public String getOperationName() {
		return operationName;
	}

	/**
	 * @return the operationStartTime
	 */
	public int getOperationStartTime() {
		return operationStartTime;
	}

	/**
	 * @return the operationType
	 */
	public ResourceType getOperationType() {
		return operationType;
	}
	
	/**
	 * @return the parents
	 */
	public Set<OperationNode> getParents() {
		return parents;
	}

	/**
	 * @return the slack
	 */
	public int getSlack() {
		return slack;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((operationName == null) ? 0 : operationName.hashCode());
		return result;
	}

	/**
	 * @param edges
	 *            the edges to set
	 */
	public void setEdges(Set<OperationEdge> edges) {
		this.edges = edges;
	}

	/**
	 * @param executionStatus the executionStatus to set
	 */
	public void setExecutionStatus(ExecutionStatus executionStatus) {
		this.executionStatus = executionStatus;
	}
	
	/**
	 * @param operationEndTime
	 *            the operationEndTime to set
	 */
	public void setOperationEndTime(int operationEndTime) {
		this.operationEndTime = operationEndTime;
	}

	/**
	 * @param operationStartTime
	 *            the operationStartTime to set
	 */
	public void setOperationStartTime(int operationStartTime) {
		this.operationStartTime = operationStartTime;
	}

	/**
	 * @param parents
	 *            the parents to set
	 */
	public void setParents(Set<OperationNode> parents) {
		this.parents = parents;
	}

	/**
	 * @param slack the slack to set
	 */
	public void setSlack(int slack) {
		this.slack = slack;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
        @Override
	public String toString() {
		//return "OperationNode [operationName=" + operationName + ", operationType=" + operationType + "]";
		return "OperationNode [operationName=" + operationName + ", slack=" + slack + "]";
	}
}
