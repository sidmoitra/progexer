/*
 * Copyright 2016 Siddhartha Moitra.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sid.codedesign.progexer.core.schedule.model;

import com.sid.codedesign.progexer.core.parse.ProblemDescription;
import com.sid.codedesign.progexer.core.schedule.OperationNameComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Siddhartha Moitra
 */
public class ScheduleResult {

	private String problemDescriptionFile;
	private ProblemDescription problemDescription;
	private final List<OperationNode> asapSchedule;
	private final List<OperationNode> alapSchedule;
	private final List<OperationNode> listSchedule;
	private final List<OperationNode> ilpSchedule;
	private final List<DesignExplorationResult> explorationResults;

	public ScheduleResult() {
		this.asapSchedule = new ArrayList<>();
		this.alapSchedule = new ArrayList<>();
		this.listSchedule = new ArrayList<>();
		this.ilpSchedule = new ArrayList<>();
		this.explorationResults = new ArrayList<>();
	}

	public List<OperationNode> getAlapSchedule() {
		return alapSchedule;
	}

	public List<OperationNode> getAsapSchedule() {
		return asapSchedule;
	}

	public List<DesignExplorationResult> getExplorationResults() {
		return explorationResults;
	}

	public List<OperationNode> getIlpSchedule() {
		return ilpSchedule;
	}

	public List<OperationNode> getListSchedule() {
		return listSchedule;
	}

	public ProblemDescription getProblemDescription() {
		return problemDescription;
	}

	public String getProblemDescriptionFile() {
		return problemDescriptionFile;
	}

	public void setAlapSchedule(List<OperationNode> alapSchedule) {
		if (alapSchedule != null && alapSchedule.size() > 0) {
			this.alapSchedule.addAll(alapSchedule);
			Collections.sort(this.alapSchedule, new OperationNameComparator());
		}
	}

	public void setAsapSchedule(List<OperationNode> asapSchedule) {
		if (asapSchedule != null && asapSchedule.size() > 0) {
			this.asapSchedule.addAll(asapSchedule);
			Collections.sort(this.asapSchedule, new OperationNameComparator());
		}
	}

	public void setExplorationResults(List<DesignExplorationResult> explorationResults) {
		if (explorationResults != null && explorationResults.size() > 0) {
			this.explorationResults.addAll(explorationResults);
		}
	}

	public void setIlpSchedule(List<OperationNode> ilpSchedule) {
		if (ilpSchedule != null && ilpSchedule.size() > 0) {
			this.ilpSchedule.addAll(ilpSchedule);
			Collections.sort(this.ilpSchedule, new OperationNameComparator());
		}
	}

	public void setListSchedule(List<OperationNode> listSchedule) {
		if (listSchedule != null && listSchedule.size() > 0) {
			this.listSchedule.addAll(listSchedule);
			Collections.sort(this.listSchedule, new OperationNameComparator());
		}
	}

	public void setProblemDescription(ProblemDescription problemDescription) {
		this.problemDescription = problemDescription;
	}

	public void setProblemDescriptionFile(String problemDescriptionFile) {
		this.problemDescriptionFile = problemDescriptionFile;
	}
}
